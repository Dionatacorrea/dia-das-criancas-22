import React from "react";

import logo2 from "../img/logo2.png";

export const Footer = () => (
  <footer className="footer center">
    <h4 className="footer__brand primary">Ministério Vida do Reino</h4>
    <img className="footer__logo" src={logo2} />
  </footer>
);
