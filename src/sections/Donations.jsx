import React from "react";

const getClassName = (percent) => {
  if (percent < 50) return "low";
  if (percent < 80) return "medium";
  if (percent < 99) return "high";
  return "reached";
};

const Money = ({ item }) => (
  <div className="n n--money">
    <div className="n__label">doado</div>
    <div className="n__number">
      <small>R$</small> {item.isMoney}
    </div>
  </div>
);

const Normal = ({ item }) => {
  const percent = Number((100 / +item.goal) * +item.reached).toFixed(0);
  return (
    <>
      <div className="n">
        <div className="n__label primary">meta</div>
        <div className="n__number primary">{item.goal}</div>
      </div>
      <div className="n">
        <div className="n__label">doado</div>
        <div className="n__number">{item.reached}</div>
      </div>
      <div className="p">
        <div className="p__percent">{percent}%</div>
        <div
          className={`p__progress ${getClassName(percent)}`}
          style={{ height: `${percent}%` }}
        ></div>
      </div>
    </>
  );
};

const Donation = ({ item }) => {
  const isMoney = item.isMoney;
  return (
    <div className="item">
      <h4 className="item__title">{item.name}</h4>
      <div className="item__info">
        {isMoney ? <Money item={item} /> : <Normal item={item} />}
      </div>
    </div>
  );
};

export const Lists = ({ listItems }) => {
  if (listItems.length === 0) return null;
  return (
    <ul className="info">
      {listItems.map((item) => (
        <li key={item.id}>
          <div className="info__item">{item.name}</div>
          <div className="info__when">{item.when}</div>
          <div className="info__money">
            <small>R$</small> {item.value}
          </div>
        </li>
      ))}
    </ul>
  );
};

export const Donations = ({ icon, title, subtitle, items, listItems }) => {
  return (
    <>
      <div className="subsection">
        <h4 className="h4">
          <span className="icon">{icon}</span>
          {title}
        </h4>
        <p className="paragraph secondary">{subtitle}</p>
        {items &&
          items.length &&
          items.map((item) => <Donation item={item} key={item.id} />)}

        {listItems && <Lists listItems={listItems} />}
      </div>
      <div></div>
    </>
  );
};
