import React from "react";

export const Infos = () => (
  <section className="section">
    <h2 className="align-center h3"> Veja as fotos do evento: 🥰</h2>

    <h4 className="contact center flex">
      <small className="primary">Acesse:</small>
      <a href="https://www.instagram.com/vidadoreino/" className="whats">
        {" "}
        https://www.instagram.com/vidadoreino/{" "}
      </a>
      <small></small>
    </h4>
  </section>
);
