export const donations = [
  {
    id: 1,
    icon: "🧸",
    title: "Brinquedos",
    subtitle: "",
    items: [
      
      {
        id: 1,
        name: "Brinquedos meninos",
        goal: 60,
        reached: 0,
      },
      {
        id: 2,
        name: "Brinquedos meninas",
        goal: 60,
        reached: 0,
      },

      
    ],
  },
  {
    id: 2,
    icon: "🍭",
    title: "Guloseimas",
    subtitle:
      "Essas guloseimas serão usadas para fazer os kits que cada criança receberá.",
    items: [
      {
        id: 1,
        name: "Bala de Goma (unidade)",
        goal: 120,
        reached: 0,
      },
      {
        id: 2,
        name: "Pirulitos (pacote 400g ou 500g)",
        goal: 15,
        reached:0,
      },
      {
        id: 3,
        name: "Pipoca doce (pacote pequeno) e Mumuzinho",
        goal: 120,
        reached: 0,
      },
      {
        id: 4,
        name: "Balas (pacote)",
        goal: 15,
        reached: 0,
      },
      {
        id: 5,
        name: "Chiclete (caixa 50un)",
        goal: 5,
        reached: 0,
      },
      {
        id: 6,
        name: "Bom Bom (unidade)",
        goal: 240,
        reached: 0,
      },
      {
        id: 7,
        name: "Paçoca (unidade embalada individual)",
        goal: 120,
        reached: 0,
      },
      {
        id: 8,
        name: "Ducrem Napolitano (unidade)",
        goal: 120,
        reached: 0,
      },
      {
        id: 9,
        name: "Pitchulinha Refrigerante (unidade)",
        goal: 120,
        reached: 0,
      },
    ],
  },
  {
    id: 3,
    icon: "🍔",
    title: "Alimentos",
    subtitle:
      "Alimentos que serão servidos no evento: pipoca e suco, e para levar para casa os cachorros quentes.",
    items: [
      {
        id: 1,
        name: "Pipoca (pacote)",
        goal: 10,
        reached: 0,
      },
      {
        id: 2,
        name: "Suco Tang Laranja (caixa)",
        goal: 5,
        reached: 0,
      },
      {
        id: 3,
        name: "Pão Massinha (tamanho normal)",
        goal: 240,
        reached: 0,
      },
      {
        id: 4,
        name: "Salsicha (Kg)",
        goal: 8,
        reached: 0,
      },
      {
        id: 5,
        name: "Molho de Tomate (sache 340g)",
        goal: 10,
        reached: 0,
      },
      {
        id: 6,
        name: "Batata Palha (pacote 80g)",
        goal: 5,
        reached: 0,
      },
      {
        id: 7,
        name: "Geladinho (unidade)",
        goal: 300,
        reached: 0,
      },
    ],
  },
  {
    id: 4,
    icon: "🍽 ",
    title: "Ajuda para as Famílias",
    subtitle:
      "As cestas básicas e caixas de leite serão sorteadas para as famílias.",
    items: [
      {
        id: 1,
        name: "Cesta Básica",
        goal: 10,
        reached: 0,
      },
      {
        id: 2,
        name: "Caixa de leite (12 unidades)",
        goal: 12,
        reached: 0,
      },
    ],
  },
  {
    id: 5,
    icon: "🛠️",
    title: "Diversos",
    subtitle: "",
    items: [
      {
        id: 1,
        name: "Embalagens plástica para cachorro quente",
        goal: 240,
        reached: 0,
      },
      {
        id: 2,
        name: "Balão (pacote 50 unidades)",
        goal: 10,
        reached: 0,
      },
    ],
  },
  {
    id: 6,
    icon: "💰",
    title: "Doações em dinheiro",
    subtitle:
      "Doações em dinheiro serão utilizados para compra de alguns dos items acima.",
    items: [
      {
        id: 1,
        name: "Doação em Dinheiro",
        isMoney: "",
      },
    ],
  },
  {
    id: 7,
    icon: "💰",
    title: "Compras usando as doações em dinheiro",
    subtitle: "",
    listItems: [
       {
         id: 1,
         name: "Despesas com embalagens individuais.",
         when: "",
         value: "",
       },
        
    ],
  },
];

export const heroes = {
  title: "HERÓIS QUE JÁ DOARAM 🦸🏿‍♂️🦸🏽‍♀️",
  items: [
    {
      id: 1,
      user: "https://www.instagram.com/alzenisescalcon/",
      photo: "alzenibeto.jpg",
    },
    
    
  ],
};

export const volunteers = {
  title: "VOLUNTÁRIOS 💪 🙌",
  items: [
    {
      id: 1,
      user: "https://www.instagram.com/alzenisescalcon/",
      photo: "alzenibeto.jpg",
    },
         
  ],
};
